import 'dart:async';
import 'package:flutter/material.dart';
import 'package:sepideh_jelokhani/CustomMethodes/class_data.dart';
import 'package:sepideh_jelokhani/EndPageNewWrite/product_detail_new.dart';
import 'package:sepideh_jelokhani/Screens/product_list_builder.dart';

class ProductListScreen extends StatefulWidget {
  const ProductListScreen({Key key, this.categoryId}) : super(key: key);
  final int categoryId;

  @override
  _ProductListScreenState createState() => _ProductListScreenState();
}

class _ProductListScreenState extends State<ProductListScreen> {
  ScrollController scrollController = ScrollController();
  int currentPage = 1;
  bool moreLoading = false;
  bool centerLoading = true;
  bool moreData = true;
  List productDetails = [];
  bool connection = true;

  Future getListData(int currentPage) async {
    var result = await getListProductsDetails(
        widget.categoryId, currentPage, "/api/v2/podcasts");
    if (result != false) {
      var newResult = result["data"];
      /*print(newResult);*/
      List data = newResult;
      if (data.isEmpty) {
        setState(() {
          moreData = false;
        });
      } else {
        setState(() {
          moreData = true;
        });
      }
      setState(() {
        productDetails += newResult;
        connection = true;
        centerLoading = false;
        moreLoading = false;
      });
    } else {
      setState(() {
        connection = false;
        centerLoading = false;
        moreLoading = false;
      });
    }
  }

  updateProductDetail() async {
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (moreData) {
          setState(() {
            moreLoading = true;
          });
        } else {
          setState(() {
            moreLoading = false;
          });
        }
        setState(() {
          currentPage++;
        });
        getListData(currentPage);
      }
    });
  }

  @override
  void initState() {
    updateProductDetail();
    getListData(currentPage);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(210, 40, 187, 164),
        leading: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Image.asset("images/sepideh.png"),
        ),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ))
        ],
      ),
      body: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
                image: AssetImage("images/flower_back1.jpg"),
                alignment: Alignment.bottomLeft)),
        child: bodyView(),
      ),
    );
  }

  bodyView() {
    if (centerLoading) {
      return const Center(
        child: RefreshProgressIndicator(),
      );
    } else if (!connection && productDetails.isEmpty) {
      return Center(
          child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Image.asset("images/noInternet1.png"),
          const Text(
            "خطای اتصال به اینترنت",
          ),
          const SizedBox(
            height: 10,
          ),
          ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10))),
              onPressed: () {
                setState(() {
                  centerLoading = true;
                });
                getListData(currentPage);
              },
              child: const Padding(
                padding: EdgeInsets.all(8.0),
                child: Text(
                  "تلاش دوباره",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ))
        ],
      ));
    } else {
      return Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              "images/header.jpg",
              fit: BoxFit.cover,
            ),
          ),
          Expanded(
              child: ProductListBuilder(
            productDetails: productDetails,
            scrollController: scrollController,
          )),
          moreLoading
              ? const Padding(
                  padding: EdgeInsets.all(8.0),
                  child: RefreshProgressIndicator(),
                )
              : Container(
                  height: 0.0,
                )
        ],
      );
    }
  }
}
