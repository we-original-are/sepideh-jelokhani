import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:sepideh_jelokhani/Auth/auth_screen.dart';
import 'package:sepideh_jelokhani/CustomMethodes/class_data.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_loading_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_text_field.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custome_message_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'package:intl/intl.dart';

class AccountScreen extends StatefulWidget {
  const AccountScreen({Key key}) : super(key: key);

  @override
  _AccountScreenState createState() => _AccountScreenState();
}

class _AccountScreenState extends State<AccountScreen> {
  String fullName = "";
  String userNumber = "";
  String profileUrl = "";
  int humanType = 2;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _familyController = TextEditingController();
  String _name = "";
  String _family = "";
  int _humanType = 2;

  @override
  void initState() {
    getUserAccountDataPrefs();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              headerUserCard(),
              Container(
                padding: const EdgeInsets.all(10),
                width: MediaQuery.of(context).size.width,
                margin: const EdgeInsets.all(5.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    border: Border.all(
                      color: const Color.fromARGB(210, 40, 187, 164),
                    )),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      "ویرایش اطلاعات کاربری",
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                    const Divider(
                      color: Color.fromARGB(255, 202, 78, 148),
                      thickness: 1.2,
                    ),
                    CustomAccountTextField(
                      hintText: " نام خود را وارد کنید",
                      controller: _nameController,
                      labelText: "نام:",
                      textInputAction: TextInputAction.next,
                    ),
                    CustomAccountTextField(
                      hintText: " نام خانوادگی خود را وارد کنید",
                      controller: _familyController,
                      labelText: "نام خانوادگی:",
                      textInputAction: TextInputAction.next,
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(vertical: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(
                            color: const Color.fromARGB(210, 40, 187, 164),
                          )),
                      child: PopupMenuButton(
                          onSelected: (int selectedValue) {
                            setState(() {
                              humanType = selectedValue;
                            });
                          },
                          initialValue: humanType,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0)),
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                horizontal: 10, vertical: 20),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                const Text(
                                  "جنسیت:",
                                  style: TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  humanType == 1
                                      ? "آقا"
                                      : humanType == 0
                                          ? "خانم"
                                          : "انتخاب کنید",
                                  style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          itemBuilder: (BuildContext context) => [
                                const PopupMenuItem(
                                  value: 1,
                                  child: Text("آقا"),
                                ),
                                const PopupMenuItem(
                                  value: 0,
                                  child: Text("خانم"),
                                ),
                              ].toList()),
                    ),
                    Center(
                        child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          primary: const Color.fromARGB(210, 40, 187, 164),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0))),
                      child: const Text(
                        "ذخیره",
                        style: TextStyle(
                            fontSize: 18.0, fontWeight: FontWeight.bold),
                      ),
                      onPressed: () async {
                        if (_nameController.text.trim().isEmpty &&
                            _familyController.text.trim().isEmpty) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return const CustomMessageDialog(
                                    message:
                                        "لطفا ابتدا نام و نام خانوادگی خود را وارد کنید");
                              });
                        } else if (_nameController.text.trim().isEmpty) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return const CustomMessageDialog(
                                    message: " نام نمی تواند خالی باشد");
                              });
                        } else if (_familyController.text.trim().isEmpty) {
                          showDialog(
                              context: context,
                              builder: (context) {
                                return const CustomMessageDialog(
                                    message:
                                        " نام خانوادگی نمی تواند خالی باشد");
                              });
                        } else {
                          setState(() {
                            _name = _nameController.text.trim();
                            _family = _familyController.text.trim();
                            _humanType = humanType;
                            fullName = _name + " " + _family;
                          });
                          var response = await updateCurrentUserProfile(
                              context, _name, _family, _humanType);
                          if (response) {
                            fullName = _name + " " + _family;
                            humanType = _humanType;
                          }
                        }
                      },
                    ))
                  ],
                ),
              ),
              const Center(
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text(
                    "نسخه برنامه: ",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  headerUserCard() {
    return Card(
      margin: const EdgeInsets.all(5.0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      color: const Color.fromARGB(255, 202, 78, 148),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                GestureDetector(
                  child: profileUrl.isEmpty
                      ? const Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                          size: 50,
                        )
                      : CircleAvatar(
                          backgroundColor:
                              const Color.fromARGB(210, 40, 187, 164),
                          radius: 35,
                          backgroundImage: NetworkImage(profileUrl),
                        ),
                  onTap: () {
                    displayBottomSheet();
                  },
                ),
                const SizedBox(
                  width: 20,
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Icon(
                          Icons.person,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width / 2,
                          child: Text(
                            fullName,
                            style: const TextStyle(
                                color: Colors.white,
                                fontSize: 17,
                                fontWeight: FontWeight.bold),
                          ),
                        )
                      ],
                    ),
                    const SizedBox(height: 5.0,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        const Icon(
                          Icons.phone,
                          color: Colors.white,
                        ),
                        Text(
                          userNumber,
                          style: const TextStyle(
                              color: Colors.white,
                              fontSize: 17,
                              fontWeight: FontWeight.bold),
                        )
                      ],
                    ),
                  ],
                )
              ],
            ),
            GestureDetector(
              onTap: () {
                _showExitDialog();
              },
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: const [
                  Text(
                    "خروج",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 17,
                        fontWeight: FontWeight.bold),
                  ),
                  Icon(
                    Icons.logout,
                    color: Colors.white,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _showExitDialog() {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            titlePadding: EdgeInsets.zero,
            title: Container(
                padding: const EdgeInsets.all(10),
                decoration: const BoxDecoration(
                    color: Color.fromARGB(240, 40, 187, 164),
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(10),
                        topLeft: Radius.circular(10))),
                child: const Text(
                  "خروج",
                  style: TextStyle(color: Colors.white),
                )),
            content: const Text("آیا میخواهید از برنامه خارج شوید؟"),
            actions: [
              TextButton(
                  onPressed: () async {
                    bool result = await deletePrefsDataPrefs();
                    if (result) {
                      Navigator.pop(context);
                      Route route =
                          MaterialPageRoute(builder: (BuildContext context) {
                        return const AuthScreen();
                      });
                      Navigator.pushAndRemoveUntil(
                          context, route, (route) => false);
                    }
                  },
                  child: const Text("بله")),
              TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text("لغو")),
            ],
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
          );
        });
  }

  void getUserAccountDataPrefs() async {
    String result = await getUserMobilePrefs();
    var response = await getUserProfileDataPrefs();
    setState(() {
      String name = response['name'].toString();
      String family = response['family'].toString();
      humanType = response['humanType'];
      profileUrl = response['imageUrl'].toString() ==
              "https://courses.sepidehjelokhani.com/img/male-default.jpg"
          ? ""
          : response['imageUrl'].toString();
      fullName = (name.isEmpty || name == "null" ? "" : name) +
          " " +
          (family.isEmpty || family == "null" ? "" : family);
    });
    if (result.isNotEmpty) {
      setState(() {
        userNumber = result;
      });
    }
  }

  void displayBottomSheet() {
    showModalBottomSheet(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10), topRight: Radius.circular(10))),
        context: context,
        builder: (context) {
          return SizedBox(
            height: MediaQuery.of(context).size.height / 6,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                ListTile(
                  shape: const RoundedRectangleBorder(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10))),
                  title: const Text(
                    "گالری",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  leading: const Icon(
                    Icons.photo_library,
                    color: Colors.teal,
                  ),
                  onTap: () {
                    _getImage(ImageSource.gallery);
                  },
                ),
                ListTile(
                  title: const Text(
                    "دوربین",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  leading: const Icon(
                    Icons.linked_camera,
                    color: Colors.teal,
                  ),
                  onTap: () {
                    _getImage(ImageSource.camera);
                  },
                ),
              ],
            ),
          );
        });
  }

  Future _getImage(ImageSource imageSource) async {
    PickedFile imageFile = await ImagePicker.platform
        .pickImage(source: imageSource, imageQuality: 100);
    if (imageFile != null) {
      File image = File(imageFile.path);
      Navigator.pop(context);
      List<int> imageBytes = await image.readAsBytes();
      String base64Image = base64Encode(imageBytes);
      var response = await updateProfileImage(context, base64Image);
      if (response) {
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
              'پروفایل با موفقیت بروز رسانی شد',
              style: TextStyle(color: Colors.black),
            ),
            backgroundColor: Color.fromARGB(240, 40, 187, 164),
          ),
        );
        getUserAccountDataPrefs();
      }
    }
  }
}
