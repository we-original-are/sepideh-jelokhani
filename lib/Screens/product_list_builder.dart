import 'package:flutter/material.dart';
import 'package:sepideh_jelokhani/EndPageNewWrite/product_detail_new.dart';

class ProductListBuilder extends StatefulWidget {
  const ProductListBuilder(
      {Key key, this.productDetails, this.scrollController})
      : super(key: key);
  final List<dynamic> productDetails;
  final ScrollController scrollController;

  @override
  _ProductListBuilderState createState() => _ProductListBuilderState();
}

class _ProductListBuilderState extends State<ProductListBuilder> {
  List<dynamic> productDetails = [];

  setData() async {
    productDetails = widget.productDetails;
  }

  @override
  void initState() {
    setData();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant ProductListBuilder oldWidget) {
    setData();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: GridView.builder(
          controller: widget.scrollController,
          padding: const EdgeInsets.all(8.0),
          itemCount: productDetails.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              mainAxisSpacing: 10,
              crossAxisSpacing: 10,
              childAspectRatio: 1),
          itemBuilder: (context, index) {
            return GestureDetector(
                onTap: () {
                  Route route = MaterialPageRoute(builder: (context) {
                    return ProductDetailNew(
                      index: index,
                      productDetails: productDetails,
                    );
                  });
                  Navigator.push(context, route);
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      border: Border.all(
                          color: const Color.fromARGB(210, 40, 187, 164),
                          width: 1.5)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: ClipRRect(
                          child: Container(
                            padding: const EdgeInsets.all(8.0),
                            child: productDetails[index]['image']
                                        .toString()
                                        .isNotEmpty &&
                                    productDetails[index]['image'].toString() !=
                                        "null"
                                ? Image.network(
                                    productDetails[index]['image'],
                                  )
                                : Image.asset("images/videocast_default.png"),
                          ),
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        padding: const EdgeInsets.all(4.0),
                        child: SizedBox(
                          child: Text(
                            productDetails[index]['name'].toString().length > 35
                                ? productDetails[index]['name']
                                        .toString()
                                        .substring(0, 35) +
                                    " ..."
                                : productDetails[index]['name'].toString(),
                            style: const TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                          width: MediaQuery.of(context).size.width,
                        ),
                        decoration: const BoxDecoration(
                            color: Color.fromARGB(230, 40, 187, 164),
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(8.0),
                                bottomRight: Radius.circular(8.0))),
                      )
                    ],
                  ),
                ));
          }),
    );
  }
}
