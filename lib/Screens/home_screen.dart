import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:sepideh_jelokhani/CustomMethodes/class_data.dart';
import 'package:sepideh_jelokhani/Screens/product_list_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<AppData> homeList = [];
  bool connection = true;

  Future<void> getListData() async {
    var result = await getListCategories("/api/v2/podcasts-categories");
    if (result != false) {
      setState(() {
        homeList = result;
        connection = true;
      });
    } else {
      setState(() {
        connection = false;
      });
    }
  }

  @override
  void initState() {
    getListData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white,
        body: connection
            ? Container(
                decoration: const BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage("images/flower_back1.jpg"),
                        alignment: Alignment.bottomLeft)),
                child: homeList.isNotEmpty
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: width,
                            child: Image.asset(
                              "images/header.jpg",
                              fit: BoxFit.cover,
                            ),
                          ),
                          Expanded(
                              child: GridView.builder(
                                  itemCount: homeList.length,
                                  padding: const EdgeInsets.all(8.0),
                                  gridDelegate:
                                      const SliverGridDelegateWithFixedCrossAxisCount(
                                          crossAxisCount: 2,
                                          mainAxisSpacing: 10,
                                          crossAxisSpacing: 10,
                                          childAspectRatio: 1.75),
                                  itemBuilder: (context, index) {
                                    final Widget svg = SvgPicture.network(
                                      homeList[index].image,
                                      alignment: Alignment.bottomCenter,
                                    );
                                    return GestureDetector(
                                        onTap: () {
                                          Route route = MaterialPageRoute(
                                              builder: (context) {
                                            return ProductListScreen(
                                                categoryId: homeList[index].id);
                                          });
                                          Navigator.push(context, route);
                                        },
                                        child: svg);
                                  }))
                        ],
                      )
                    : const Center(
                        child: RefreshProgressIndicator(),
                      ),
              )
            : Center(
                child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Image.asset("images/noInternet1.png"),
                  const Text(
                    "خطای اتصال به اینترنت",
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10))),
                      onPressed: () {
                        setState(() {
                          connection = false;
                          homeList = [];
                        });
                        getListData();
                      },
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: Text(
                          "تلاش دوباره",
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 17),
                        ),
                      ))
                ],
              )));
  }
}
