import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'account_screen.dart';
import 'home_screen.dart';

class StartScreen extends StatefulWidget {
  const StartScreen({Key key}) : super(key: key);

  @override
  _StartScreenState createState() => _StartScreenState();
}

class _StartScreenState extends State<StartScreen> {
  int page = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(210, 40, 187, 164),
        leading: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Image.asset("images/sepideh.png"),
        ),
      ),
      body: bodyView(),
      bottomNavigationBar: CurvedNavigationBar(
          height: 55,
          index: page,
          color: const Color.fromARGB(210, 40, 187, 164),
          backgroundColor: Colors.white,
          onTap: (index) {
            setState(() {
              page = index;
            });
          },
          items: const [
            Icon(Icons.home, color: Colors.white),
            Icon(Icons.person, color: Colors.white),
          ]),
    );
  }

  bodyView() {
    if (page == 0) {
      return const HomeScreen();
    } else if (page == 1) {
      return const AccountScreen();
    }
  }
}
