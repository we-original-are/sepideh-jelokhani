import 'dart:io';
import 'package:chewie/chewie.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:video_player/video_player.dart';

class VideoManagerNew extends StatefulWidget {
  const VideoManagerNew(
      {Key key,
      this.file,
      this.title,
      this.productContexts,
      this.index,
      this.listMediaFile})
      : super(key: key);
  final File file;
  final String title;
  final List<dynamic> productContexts;
  final int index;
  final List<String> listMediaFile;

  @override
  _VideoManagerNewState createState() => _VideoManagerNewState();
}

class _VideoManagerNewState extends State<VideoManagerNew> {
  VideoPlayerController _videoPlayerController;
  ChewieController _chewieController;

  List productContexts = [];
  List<String> listMediaFile = [];
  bool isDownloaded = false;
  Directory directory = Directory("");
  Dio dio = Dio();

  double progress = 0.0;
  int newProgress = 0;
  bool loadingDownload = false;
  String downloadingPathTitle = "";

  Future setData() async {
    productContexts = widget.productContexts[widget.index]['contexts'];
    directory = await getDirectory();
    listMediaFile = widget.listMediaFile;
    await getVideoListPath();
  }

  @override
  void initState() {
    setData();
    getFilesFromDirectory();
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController.pause();
    _chewieController.pause();
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant VideoManagerNew oldWidget) {
    getVideoListPath();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return isDownloaded
        ? Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width - 10,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.title,
                    style: const TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height / 4,
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Chewie(controller: _chewieController)),
              ),
            ],
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width - 10,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    widget.title,
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        fontSize: 17, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  videoManager(widget.index);
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 4,
                  decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  child: managerDownloadProgress(),
                ),
              ),
            ],
          );
  }

  initialVideoPlayer(File file) async {
    _videoPlayerController = VideoPlayerController.file(file)..initialize();
    _chewieController = ChewieController(
        videoPlayerController: _videoPlayerController,
        looping: false,
        autoPlay: true,
        aspectRatio: 3 / 2,
        autoInitialize: true,
        customControls: CupertinoControls(
            backgroundColor: Colors.black.withOpacity(.6),
            iconColor: Colors.white),
        cupertinoProgressColors: ChewieProgressColors(
          playedColor: Colors.blue,
          bufferedColor: Colors.white38,
          handleColor: Colors.blue,
        ),
        placeholder: Container(
          color: Colors.black,
        ));
  }

  Future getVideoListPath() async {
    for (String path in listMediaFile) {
      if (widget.file.path == path) {
        initialVideoPlayer(widget.file);
        setState(() {
          isDownloaded = true;
        });
      }
    }
  }

  managerDownloadProgress() {
    String pathTitle = getVideoPathTitleFromMediaLink(widget.index);
    if (loadingDownload &&
        downloadingPathTitle.split('.mp4').first == pathTitle) {
      return progressDownloader();
    } else {
      return const Icon(
        Icons.arrow_circle_down_outlined,
        size: 30,
        color: Colors.white,
      );
    }
  }

  progressDownloader() {
    return CircularPercentIndicator(
      radius: 34.0,
      lineWidth: 3.65,
      percent: progress,
      center: Text(
        "$newProgress%",
        style: const TextStyle(fontSize: 12.3, color: Colors.white),
      ),
      progressColor: Colors.blue,
      backgroundColor: Colors.white,
    );
  }

  Future videoManager(int index) async {
    String pathTitle = getVideoPathTitleFromMediaLink(index) + ".mp4";
    String mediaLink = productContexts[index]['media_link'].toString();
    File file = File(directory.path + pathTitle);
    if (await file.exists()) {
      setState(() {
        isDownloaded = true;
        initialVideoPlayer(widget.file);
      });
    } else {
      var result = await downloadFile(index, pathTitle, mediaLink);
      if (result != null && result) {
        setState(() {
          isDownloaded = true;
          initialVideoPlayer(widget.file);
        });
      }
    }
  }

  Future downloadFile(int index, String pathTitle, String mediaLink) async {
    setState(() {
      loadingDownload = true;
    });
    bool downloaded = await saveFile(index, pathTitle, mediaLink);
    if (downloaded) {
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
      await getFilesFromDirectory();
      return true;
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'دانلود ناموفق بود',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Color.fromARGB(240, 40, 187, 164),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
      return false;
    }
  }

  Future saveFile(int index, String pathTitle, String mediaLink) async {
    File file = File(directory.path + pathTitle);
    setState(() {
      downloadingPathTitle = pathTitle;
    });
    try {
      await dio.download(mediaLink, file.path,
          onReceiveProgress: (downloaded, totalSize) {
        setState(() {
          progress = downloaded / totalSize;
          newProgress = (progress * 100).toInt();
        });
      });
      return true;
    } catch (e) {
      await deleteFileFromDirectory(file);
      return false;
    }
  }

  getVideoPathTitleFromMediaLink(int index) {
    var videoPathTitle = productContexts[index]['media_link']
        .toString()
        .split('/')
        .last
        .split('.mp4')
        .first;
    return videoPathTitle;
  }

  Future getFilesFromDirectory() async {
    Directory _directory = await getDirectory();
    String dir = _directory.path.split("files").first;
    Directory directory = Directory(dir);
    List<FileSystemEntity> _files;
    _files = directory.listSync(recursive: true, followLinks: false);
    for (FileSystemEntity entity in _files) {
      String path = entity.path;
      if (path.endsWith('.mp4') || path.endsWith('.mp3')) {
        setState(() {
          listMediaFile.add(path);
        });
      }
    }
    await getVideoListPath();
  }

  Future getDirectory() async {
    Directory directory = await getExternalStorageDirectory();
    return directory;
  }

  Future deleteFileFromDirectory(File file) async {
    try {
      await file.delete();
    } catch (e) {
      print(e.toString());
    }
  }
}
