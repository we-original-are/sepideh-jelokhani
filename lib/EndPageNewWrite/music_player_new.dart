import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:io';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter_media_notification/flutter_media_notification.dart';
import 'package:path_provider/path_provider.dart';

enum PlayerState { stopped, playing, paused }

class MusicPlayerNew extends StatefulWidget {
  const MusicPlayerNew(
      {Key key, this.productContexts, this.currentIndex, this.playerOn})
      : super(key: key);

  final List<dynamic> productContexts;
  final int currentIndex;
  final bool playerOn;

  @override
  _MusicPlayerNewState createState() => _MusicPlayerNewState();
}

class _MusicPlayerNewState extends State<MusicPlayerNew> {
  /// //////////////////////////
  /// Todo: list of variables
  /// //////////////////////////
  setData() async {
    productContexts = widget.productContexts;
    directory = await getDirectory();
  }

  Future getDirectory() async {
    Directory directory = await getExternalStorageDirectory();
    return directory;
  }

  AudioPlayer audioPlayer = AudioPlayer();
  Directory directory = Directory("");

  List productContexts = [];

  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText =>
      duration != null ? duration.toString().split('.').first : '';

  get positionText =>
      position != null ? position.toString().split('.').first : '';

  bool isMuted = false;
  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;
  Duration duration = const Duration();
  Duration position = const Duration();

  double progress = 0.0;
  int newProgress = 0;
  bool loadingDownload = false;
  String downloadingPathTitle = "";
  String playingTitle = "";
  String localFilePath = "";
  int currentIndex = 0;
  String currentMusicUrl = "";
  bool playerOn = false;

  @override
  void initState() {
    setData();
    hideNotification();
    setupNotification();
    initAudioPlayer();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MusicPlayerNew oldWidget) {
    setNewPlayIndex();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _positionSubscription.cancel();
    _audioPlayerStateSubscription.cancel();
    audioPlayer.stop();
    hideNotification();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    setAutomaticPlayer();
    return playerOn
        ? Container(
            decoration: const BoxDecoration(
                color: Color.fromARGB(240, 40, 187, 164),
                boxShadow: [
                  BoxShadow(color: Colors.white, blurRadius: 8.0),
                ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 10),
                        child: SizedBox(
                          width: MediaQuery.of(context).size.width,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  const Icon(Icons.queue_music_outlined),
                                  const SizedBox(
                                    width: 10,
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width / 2,
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 5.0),
                                      child: Text(
                                        playingTitle.length > 45
                                            ? playingTitle.substring(0, 45) +
                                                " ..."
                                            : playingTitle,
                                        style: const TextStyle(
                                            fontSize: 16.0,
                                            height: 1.5,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              IconButton(
                                  tooltip: "بستن",
                                  onPressed: () {
                                    closePLayer();
                                  },
                                  icon: const Icon(
                                    Icons.close,
                                  ))
                            ],
                          ),
                        ),
                      ),
                      Slider(
                          thumbColor: Colors.black,
                          activeColor: Colors.black,
                          inactiveColor: Colors.black54,
                          value: position.inMilliseconds.toDouble(),
                          onChanged: (double value) {
                            return audioPlayer
                                .seek((value / 1000).roundToDouble())
                                .ignore();
                          },
                          min: 0.0,
                          max: duration.inMilliseconds.toDouble() +
                              1000.toDouble()),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            if (position != null)
                              Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  position != null
                                      ? "${positionText ?? ''}"
                                      : '',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            if (position != null)
                              Padding(
                                padding: const EdgeInsets.all(3.0),
                                child: Text(
                                  duration != null ? durationText : '',
                                  style: const TextStyle(
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      musicPlayerController(),
                    ],
                  ),
                ),
              ],
            ),
          )
        : const Text(
            "",
            style: TextStyle(fontSize: 0.0),
          );
  }

  /// ////////////////////////////////////////
  /// Todo: Notification Controller methods
  /// ////////////////////////////////////////

  Widget musicPlayerController() {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            isMuted ? mute(false) : mute(true);
          },
          icon: isMuted
              ? const Icon(
                  Icons.volume_off,
                )
              : const Icon(
                  Icons.volume_up,
                ),
        ),
        IconButton(
            tooltip: "بعدی",
            onPressed: () {
              nextAction();
            },
            icon: const Icon(
              Icons.skip_next,
            )),
        IconButton(
          tooltip: "توقف",
          onPressed: () async {
            if (isPlaying || isPaused) {
              await stop();
            }
          },
          icon: const Icon(
            Icons.stop,
          ),
        ),
        IconButton(
            onPressed: () {
              playAndPauseAction();
            },
            icon: isPlaying
                ? const Icon(
                    Icons.pause,
                  )
                : const Icon(
                    Icons.play_arrow,
                  )),
        IconButton(
            tooltip: "قبلی",
            onPressed: () {
              previousAction();
            },
            icon: const Icon(
              Icons.skip_previous,
            )),
      ],
    );
  }

  Future<void> hideNotification() async {
    await MediaNotification.hideNotification();
  }

  showNotificationWhenPlaying(String title, String singer) async {
    MediaNotification.showNotification(
        title: title, author: singer, isPlaying: true);
  }

  showNotificationWhenPausing(String title, String singer) async {
    MediaNotification.showNotification(
        title: title, author: singer, isPlaying: false);
  }

  Future<void> setNotificationPlayAndPauseIcon() async {
    String singer = "";
    isPlaying
        ? showNotificationWhenPlaying(playingTitle, singer)
        : showNotificationWhenPausing(playingTitle, singer);
  }

  Future<void> setupNotification() async {
    await MediaNotification.setListener('pause', () async {
      await pause();
    });

    await MediaNotification.setListener('play', () async {
      await playLocal();
    });

    await MediaNotification.setListener('next', () async {
      nextAction();
    });

    await MediaNotification.setListener('prev', () async {
      previousAction();
    });
  }

  /// ////////////////////////////////////////
  /// Todo: Audio Player methods
  /// ////////////////////////////////////////
  void initAudioPlayer() async {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
        setState(() => duration = audioPlayer.duration);
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = PlayerState.stopped;
        duration = const Duration(seconds: 0);
        position = const Duration(seconds: 0);
      });
    });
  }

  Future playLocal() async {
    await audioPlayer.play(localFilePath, isLocal: true);
    setState(() {
      playerState = PlayerState.playing;
      setNotificationPlayAndPauseIcon();
    });
  }

  Future pause() async {
    await audioPlayer.pause();
    setState(() {
      playerState = PlayerState.paused;
      setNotificationPlayAndPauseIcon();
    });
  }

  checkThisIsMusic(int index) {
    String mediaLink = productContexts[index]["media_link"].toString();
    if (mediaLink.endsWith('.mp3')) {
      return true;
    } else {
      return false;
    }
  }

  getMediaName(int index) {
    String mTitle = productContexts[index]['media_name'].toString();
    String mediaTitle =
        (mTitle.isNotEmpty && mTitle != "null") ? mTitle : "بدون نام";
    return mediaTitle;
  }

  nextAction() async {
    if (currentIndex == productContexts.length - 1) {
      setState(() {
        currentIndex = 0;
      });
      for (int i = currentIndex; (i <= productContexts.length - 1); i++) {
        if (productContexts[i]['type'] == 'media' && checkThisIsMusic(i)) {
          setState(() {
            currentIndex = i;
            playingTitle = getMediaName(i);
            currentMusicUrl = productContexts[i]['media_link'].toString();
          });
          stop();
          playerManagerForNextAndPrev(true);
          break;
        }
      }
    } else {
      setState(() {
        currentIndex++;
      });
      for (int i = currentIndex; (i <= productContexts.length - 1); i++) {
        if (productContexts[i]['type'] == 'media' && checkThisIsMusic(i)) {
          setState(() {
            currentIndex = i;
            playingTitle = getMediaName(i);
            currentMusicUrl = productContexts[i]['media_link'].toString();
          });
          stop();
          playerManagerForNextAndPrev(true);
          break;
        }
      }
    }
  }

  previousAction() async {
    if (currentIndex == 0) {
      setState(() {
        currentIndex = productContexts.length - 1;
      });
      for (int i = currentIndex; i >= 0; i--) {
        if (productContexts[i]['type'] == 'media' && checkThisIsMusic(i)) {
          setState(() {
            currentIndex = i;
            playingTitle = getMediaName(i);
            currentMusicUrl = productContexts[i]['media_link'].toString();
          });
          stop();
          playerManagerForNextAndPrev(false);
          break;
        }
      }
    } else {
      setState(() {
        currentIndex--;
      });

      for (int i = currentIndex; i >= 0; i--) {
        if (productContexts[i]['type'] == 'media' && checkThisIsMusic(i)) {
          setState(() {
            currentIndex = i;
            playingTitle = getMediaName(i);
            currentMusicUrl = productContexts[i]['media_link'].toString();
          });
          stop();
          playerManagerForNextAndPrev(false);
          break;
        }
      }
    }
  }

  Future<void> playerManagerForNextAndPrev(bool next) async {
    String pathTitle = getMusicPathTitleFromMediaLink(currentIndex);
    setState(() {
      playingTitle = getMediaName(currentIndex);
    });
    File file = File(directory.path + "$pathTitle.mp3");
    if (await file.exists()) {
      if (pathTitle + ".mp3" == downloadingPathTitle && loadingDownload) {
        print("download currently");
      } else {
        setState(() {
          localFilePath = file.path;
        });
        await playLocal();
      }
    } else if (next) {
      nextAction();
      print("go to next");
    } else {
      previousAction();
      print("go to prev");
    }
  }

  void playAndPauseAction() async {
    String pathTitle = getMusicPathTitleFromMediaLink(currentIndex);
    setState(() {
      playingTitle = getMediaName(currentIndex);
    });
    File file = File(directory.path + "$pathTitle.mp3");
    if (await file.exists()) {
      if (pathTitle + ".mp3" == downloadingPathTitle && loadingDownload) {
        print("download currently");
      } else {
        setState(() {
          localFilePath = file.path;
        });
        isPlaying ? await pause() : await playLocal();
      }
    } else {
      print("file not found");
    }
  }

  Future stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = PlayerState.stopped;
      setNotificationPlayAndPauseIcon();
      position = const Duration();
    });
  }

  Future mute(bool muted) async {
    await audioPlayer.mute(muted);
    setState(() {
      isMuted = muted;
    });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  getMusicPathTitleFromMediaLink(int index) {
    var musicPathTitle = productContexts[index]['media_link']
        .toString()
        .split('/')
        .last
        .split('.mp3')
        .first;
    return musicPathTitle;
  }

  setNewPlayIndex() {
    setState(() {
      playerOn = widget.playerOn;
      currentIndex = widget.currentIndex;
    });
    playerOn ? playNewMusic() : () {};
  }

  Future playNewMusic() async {
    String pathTitle = getMusicPathTitleFromMediaLink(currentIndex);
    setState(() {
      playingTitle = getMediaName(currentIndex);
    });
    File file = File(directory.path + "$pathTitle.mp3");
    if (await file.exists()) {
      if (pathTitle + ".mp3" == downloadingPathTitle && loadingDownload) {
        print("download currently");
      } else {
        setState(() {
          localFilePath = file.path;
        });
        await stop();
        await playLocal();
      }
    } else {
      print("file not found");
    }
  }

  void setAutomaticPlayer() {
    if (position != null &&
        duration != null &&
        position > const Duration(seconds: 1) &&
        position == duration) {
      setState(() {
        nextAction();
      });
    }
    if (isPlaying && !playerOn) {
      setState(() {
        playerOn = true;
      });
    }
  }

  Future closePLayer() async {
    await stop();
    hideNotification();
    setState(() {
      playerOn = false;
    });
  }
}
