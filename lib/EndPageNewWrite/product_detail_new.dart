import 'dart:async';
import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sepideh_jelokhani/EndPageNewWrite/music_manager_new.dart';
import 'package:sepideh_jelokhani/EndPageNewWrite/music_player_new.dart';
import 'package:sepideh_jelokhani/EndPageNewWrite/video_manager_new.dart';

enum PlayerState { stopped, playing, paused }

class ProductDetailNew extends StatefulWidget {
  const ProductDetailNew({Key key, this.productDetails, this.index})
      : super(key: key);
  final List<dynamic> productDetails;
  final int index;

  @override
  _ProductDetailNewState createState() => _ProductDetailNewState();
}

class _ProductDetailNewState extends State<ProductDetailNew> {
  /// //////////////////////////
  /// Todo: list of variables
  /// //////////////////////////

  Directory directory = Directory("");
  Dio dio = Dio();

  List productContexts = [];
  List<String> listMediaFile = [];

  double progress = 0.0;
  int newProgress = 0;
  bool loadingDownload = false;
  String downloadingPathTitle = "";
  String playingTitle = "";
  String localFilePath = "";
  int currentIndex = 0;
  String currentMusicUrl = "";
  bool playerOn = false;

  Future setData() async {
    productContexts = widget.productDetails[widget.index]['contexts'];
    directory = await getDirectory();
  }

  @override
  void initState() {
    setData();
    getFilesFromDirectory();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: MusicPlayerNew(
        productContexts: productContexts,
        currentIndex: currentIndex,
        playerOn: playerOn,
      ),
      appBar: AppBar(
        backgroundColor: const Color.fromARGB(210, 40, 187, 164),
        leading: Padding(
          padding: const EdgeInsets.all(3.0),
          child: Image.asset("images/sepideh.png"),
        ),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ))
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(5.0),
        child: Container(
          padding: const EdgeInsets.only(top: 5.0, left: 5.0, right: 5.0),
          decoration: BoxDecoration(
              border: Border.all(
                  color: const Color.fromARGB(210, 40, 187, 164), width: 2),
              borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(8.0),
                  topRight: Radius.circular(8.0)),
              image: const DecorationImage(
                  image: AssetImage("images/flower_back1.jpg"),
                  alignment: Alignment.bottomLeft)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height / 6,
                width: MediaQuery.of(context).size.width,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      child: Text(
                        widget.productDetails[widget.index]['name'],
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                      width: MediaQuery.of(context).size.width / 2,
                    ),
                    widget.productDetails[widget.index]['image']
                                .toString()
                                .isNotEmpty &&
                            widget.productDetails[widget.index]['image']
                                    .toString() !=
                                "null"
                        ? Image.network(
                            widget.productDetails[widget.index]['image'],
                          )
                        : Image.asset("images/videocast_default.png"),
                  ],
                ),
              ),
              Expanded(
                child: ListView.separated(
                    padding: const EdgeInsets.only(top: 8.0, bottom: 8.0),
                    itemBuilder: (context, index) {
                      return columnItems(index);
                    },
                    separatorBuilder: (context, index) {
                      return const Divider(
                        thickness: 1.2,
                        height: 25,
                        color: Color.fromARGB(210, 40, 187, 164),
                      );
                    },
                    itemCount: productContexts.length),
              )
            ],
          ),
        ),
      ),
    );
  }

  columnItems(int index) {
    if (productContexts[index]['type'] == 'text') {
      return Html(data: productContexts[index]['text'].toString());
    } else if (productContexts[index]['type'] == 'media') {
      String mediaLink = productContexts[index]["media_link"].toString();
      if (mediaLink.endsWith('.mp3')) {
        String pathTitle = getMusicPathTitleFromMediaLink(index);
        File file = File(directory.path + "$pathTitle.mp3");
        String mTitle = productContexts[index]['media_name'].toString();
        return MusicManagerNew(
          listMediaFile: listMediaFile,
          index: index,
          productContexts: productContexts,
          play: () {
            setState(() {
              currentIndex = index;
              playerOn = true;
            });
          },
          title: mTitle,
          file: file,
        );
      } else if (mediaLink.endsWith('.mp4')) {
        String pathTitle = getVideoPathTitleFromMediaLink(index);
        File file = File(directory.path + "$pathTitle.mp4");
        String mTitle = productContexts[index]['media_name'].toString();
        return VideoManagerNew(
          index: index,
          listMediaFile: listMediaFile,
          productContexts: productContexts,
          file: file,
          title: mTitle,
        );
      } else {
        return Container();
      }
    }
  }

  getMusicPathTitleFromMediaLink(int index) {
    var musicPathTitle = productContexts[index]['media_link']
        .toString()
        .split('/')
        .last
        .split('.mp3')
        .first;
    return musicPathTitle;
  }

  getVideoPathTitleFromMediaLink(int index) {
    var videoPathTitle = productContexts[index]['media_link']
        .toString()
        .split('/')
        .last
        .split('.mp4')
        .first;
    return videoPathTitle;
  }


  Future getDirectory() async {
    Directory directory = await getExternalStorageDirectory();
    return directory;
  }

  Future deleteFileFromDirectory(File file) async {
    try {
      await file.delete();
    } catch (e) {
      print(e.toString());
    }
  }

  Future getFilesFromDirectory() async {
    Directory _directory = await getDirectory();
    String dir = _directory.path.split("files").first;
    Directory directory = Directory(dir);
    List<FileSystemEntity> _files;
    _files = directory.listSync(recursive: true, followLinks: false);
    for (FileSystemEntity entity in _files) {
      String path = entity.path;
      if (path.endsWith('.mp4') || path.endsWith('.mp3')) {
        setState(() {
          listMediaFile.add(path);
        });
      }
    }
  }
}
