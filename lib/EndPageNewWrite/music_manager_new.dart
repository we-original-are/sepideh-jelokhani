import 'dart:io';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class MusicManagerNew extends StatefulWidget {
  const MusicManagerNew({
    Key key,
    this.file,
    this.title,
    this.play,
    this.index,
    this.productContexts,
    this.listMediaFile,
  }) : super(key: key);
  final File file;
  final String title;
  final dynamic play;
  final int index;
  final List<dynamic> productContexts;
  final List<String> listMediaFile;

  @override
  _MusicManagerNewState createState() => _MusicManagerNewState();
}

class _MusicManagerNewState extends State<MusicManagerNew> {
  bool isDownloaded = false;
  String musicTitle = "";
  List productContexts = [];
  List<String> listMediaFile = [];
  Directory directory = Directory("");
  Dio dio = Dio();

  double progress = 0.0;
  int newProgress = 0;
  bool loadingDownload = false;
  String downloadingPathTitle = "";
  String playingTitle = "";
  String localFilePath = "";
  int currentIndex = 0;
  String currentMusicUrl = "";
  String pathTitle = "";

  setData() async {
    musicTitle = (widget.title.isNotEmpty && widget.title != "null")
        ? widget.title
        : "بدون نام";
    productContexts = widget.productContexts;
    listMediaFile = widget.listMediaFile;
    directory = await getDirectory();
    pathTitle = getMusicPathTitleFromMediaLink(widget.index);
    await getMusicListPath();
  }

  @override
  void initState() {
    setData();
    getFilesFromDirectory();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MusicManagerNew oldWidget) {
    getMusicListPath();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return childMusicTileManage();
  }

  childMusicTileManage() {
    double width = MediaQuery.of(context).size.width;
    if (isDownloaded) {
      return Container(
          padding: const EdgeInsets.all(8.0),
          width: width,
          decoration: BoxDecoration(
              color: const Color.fromARGB(230, 40, 187, 164),
              borderRadius: BorderRadius.circular(8.0)),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Row(
                children: [
                  FloatingActionButton(
                      tooltip: "پخش",
                      mini: true,
                      splashColor: Colors.teal.shade100,
                      elevation: 5.0,
                      backgroundColor: Colors.white,
                      onPressed: widget.play,
                      child: const Icon(
                        Icons.play_circle_outlined,
                        color: Color.fromARGB(240, 40, 187, 164),
                        size: 38,
                      )),
                  const Icon(Icons.queue_music_outlined),
                  const SizedBox(
                    width: 10,
                  ),
                  SizedBox(
                    width: width / 1.5,
                    child: Text(
                      musicTitle,
                      style: const TextStyle(
                        height: 1.5,
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ));
    } else {
      return Container(
        padding: const EdgeInsets.all(8.0),
        width: width,
        decoration: BoxDecoration(
            color: const Color.fromARGB(230, 40, 187, 164),
            borderRadius: BorderRadius.circular(8.0)),
        child: Row(
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                managerDownloadProgress(),
                const SizedBox(
                  width: 10,
                ),
                const Icon(Icons.queue_music_outlined, color: Colors.black),
              ],
            ),
            const SizedBox(
              width: 10,
            ),
            SizedBox(
              width: MediaQuery.of(context).size.width / 2,
              child: Text(
                musicTitle,
                style: const TextStyle(
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      );
    }
  }

  managerDownloadProgress() {
    if (loadingDownload &&
        downloadingPathTitle.split('.mp3').first == pathTitle) {
      return progressDownloader();
    } else {
      return FloatingActionButton(
        tooltip: "دانلود",
        mini: true,
        splashColor: Colors.teal.shade100,
        elevation: 5.0,
        backgroundColor: Colors.white,
        onPressed: () {
          musicManager(widget.index);
        },
        child: const Icon(
          Icons.arrow_circle_down_outlined,
          color: Color.fromARGB(240, 40, 187, 164),
          size: 38,
        ),
      );
    }
  }

  progressDownloader() {
    return FloatingActionButton(
      onPressed: null,
      mini: true,
      splashColor: Colors.teal.shade100,
      elevation: 5.0,
      backgroundColor: Colors.white,
      child: CircularPercentIndicator(
        radius: 34.0,
        lineWidth: 4,
        percent: progress,
        center: Text(
          "$newProgress%",
          style: const TextStyle(
            fontSize: 13,
            color: Color.fromARGB(240, 40, 187, 164),
          ),
        ),
        progressColor: const Color.fromARGB(240, 40, 187, 164),
        backgroundColor: Colors.tealAccent.shade100,
      ),
    );
  }

  Future getDirectory() async {
    Directory directory = await getExternalStorageDirectory();
    return directory;
  }

  Future getMusicListPath() async {
    for (String path in listMediaFile) {
      if (widget.file.path == path) {
        setState(() {
          isDownloaded = true;
        });
      }
    }
  }

  /// ///////////////////////////////
  /// TODO: Download Manager Method
  /// ///////////////////////////////
  Future musicManager(int index) async {
    String pathTitle = getMusicPathTitleFromMediaLink(index) + ".mp3";
    String mediaLink = productContexts[index]['media_link'].toString();
    File file = File(directory.path + pathTitle);
    if (await file.exists()) {
      setState(() {
        isDownloaded = true;
      });
    } else {
      var result = await downloadFile(index, pathTitle, mediaLink);
      if (result != null && result) {
        getMusicListPath();
        setState(() {
          isDownloaded = true;
        });
      }
    }
  }

  Future downloadFile(int index, String pathTitle, String mediaLink) async {
    setState(() {
      loadingDownload = true;
    });
    bool downloaded = await saveFile(index, pathTitle, mediaLink);
    if (downloaded) {
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
      await getFilesFromDirectory();
      return true;
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          content: Text(
            'دانلود ناموفق بود',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Color.fromARGB(240, 40, 187, 164),
        ),
      );
      setState(() {
        progress = 0.0;
        newProgress = 0;
        loadingDownload = false;
      });
      return false;
    }
  }

  Future saveFile(int index, String pathTitle, String mediaLink) async {
    File file = File(directory.path + pathTitle);
    setState(() {
      downloadingPathTitle = pathTitle;
    });
    try {
      await dio.download(mediaLink, file.path,
          onReceiveProgress: (downloaded, totalSize) {
        setState(() {
          progress = downloaded / totalSize;
          newProgress = (progress * 100).toInt();
        });
      });
      return true;
    } catch (e) {
      await deleteFileFromDirectory(file);
      return false;
    }
  }

  Future deleteFileFromDirectory(File file) async {
    try {
      await file.delete();
    } catch (e) {
      print(e.toString());
    }
  }

  getMusicPathTitleFromMediaLink(int index) {
    var musicPathTitle = productContexts[index]['media_link']
        .toString()
        .split('/')
        .last
        .split('.mp3')
        .first;
    return musicPathTitle;
  }

  Future getFilesFromDirectory() async {
    Directory _directory = await getDirectory();
    String dir = _directory.path.split("files").first;
    Directory directory = Directory(dir);
    List<FileSystemEntity> _files;
    _files = directory.listSync(recursive: true, followLinks: false);
    for (FileSystemEntity entity in _files) {
      String path = entity.path;
      if (path.endsWith('.mp4') || path.endsWith('.mp3')) {
        setState(() {
          listMediaFile.add(path);
        });
      }
    }
    return true;
  }
}
