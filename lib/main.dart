import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'Auth/auth_screen.dart';
import 'Screens/start_screen.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  runApp(const MyApp());
  HttpOverrides.global = MyHttpOverrides();
}

class MyApp extends StatefulWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int status = 0;

  @override
  void initState() {
    checkToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('fa', ''),
      ],
      debugShowCheckedModeBanner: false,
      title: 'سپیده جلوخانی',
      theme: ThemeData(primarySwatch: Colors.teal, fontFamily: "fontFa"),
      home: statusPage(),
    );
  }

  void checkToken() async {
    var result = await getTokenPrefs();
    if (result.toString().isNotEmpty && result != null) {
      setState(() {
        status = 1;
      });
    } else {
      setState(() {
        status = 2;
      });
    }
  }

  statusPage() {
    if (status == 0) {
      return const SafeArea(
        child: Scaffold(
          body: Center(),
        ),
      );
    } else if (status == 1) {
      return const StartScreen();
    } else {
      return const AuthScreen();
    }
  }
}
