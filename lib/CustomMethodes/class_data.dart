import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custome_message_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'custom_loading_dialog.dart';

class AppData {
  int id = 0;
  String name = "";
  String description = "";
  String image = "";

  AppData(
      {  this.id,
        this.name,
       this.description,
       this.image});
}

/// //////////////////////////////////////////////////////////////
/// TODO: Fetch Product List & Category List Data From Server
/// //////////////////////////////////////////////////////////////

Future getListCategories(String api) async {
  List<AppData> homeList = [];
  String token = await getTokenPrefs();
  Map<String, String> headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Authorization": "Bearer" " " + token
  };
  try {
    var response = await http
        .get(Uri.https("courses.sepidehjelokhani.com", api), headers: headers);
    if (response.statusCode == 200) {
      Map m = jsonDecode(utf8.decode(response.bodyBytes));
      List data = m['data'];
      for (int i = 0; i < data.length; i++) {
        homeList.add(AppData(
            id: data[i]['id'],
            name: data[i]['name'],
            description: data[i]['description'],
            image: data[i]['image']));
      }
      return homeList;
    } else {
      return false;
    }
  } catch (e) {
    print(e.toString());
    return false;
  }
}

Future getListProductsDetails(
    int categoryId, int currentPage, String query) async {
  String token = await getTokenPrefs();
  Map<String, String> headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Authorization": "Bearer" " " + token,
  };
  var queryParameters = {
    'category_id': categoryId.toString(),
    'page': currentPage.toString()
  };

  try {
    var response = await http.get(
        Uri.https("courses.sepidehjelokhani.com", query, queryParameters),
        headers: headers);
    print(response.statusCode);
    if (response.statusCode == 200) {
      Map m = jsonDecode(utf8.decode(response.bodyBytes));
      return m;
    } else {
      return false;
    }
  } catch (e) {
    print(e.toString());
    return false;
  }
}

/// ///////////////////////////////
/// TODO: Edit Account Methods
/// //////////////////////////////
Future updateCurrentUserProfile(
    BuildContext context, String name, String family, int humanType) async {
  showDialog(
      context: context,
      builder: (context) {
        return const CustomLoadingDialog();
      });
  String token = await getTokenPrefs();
  Map<String, String> headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Authorization": "Bearer" " " + token,
  };

  try {
    var response = await http.post(
        Uri.https(
            "courses.sepidehjelokhani.com", "/api/v2/user-profile/update"),
        headers: headers,
        body: jsonEncode({
          "name": name,
          "last_name": family,
          "gender": humanType == 2 ? null : humanType
        }));
    if (response.statusCode == 200) {
      await getCurrentUserDataFromServer(context);
      return true;
    } else if (response.statusCode == 401) {
      Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
      String err = m['error'].toString();
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return CustomMessageDialog(message: err);
          });
      return false;
    } else if (response.statusCode == 201) {
      await getCurrentUserDataFromServer(context);
      return true;
    } else {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message: "متاسفانه برنامه با مشکل مواجهه شده است");
          });
      return false;
    }
  } catch (e) {
    Navigator.pop(context);
    showDialog(
        context: context,
        builder: (context) {
          return const CustomMessageDialog(
              message:
                  " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
        });
    return false;
  }
}

updateProfileImage(BuildContext context, String image) async {
  showDialog(
      context: context,
      builder: (context) {
        return const CustomLoadingDialog();
      });
  String token = await getTokenPrefs();
  Map<String, String> headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Authorization": "Bearer" " " + token,
  };

  try {
    var response = await http.post(
        Uri.https("courses.sepidehjelokhani.com",
            "/api/v2/user-profile/update-image"),
        headers: headers,
        body: jsonEncode({"image": image}));
    print(response.statusCode);
    if (response.statusCode == 200) {
      await getCurrentUserDataFromServer(context);
      return true;
    } else if (response.statusCode == 201) {
      await getCurrentUserDataFromServer(context);
      return true;
    } else if (response.statusCode == 401) {
      Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
      String err = m['error'].toString();
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return CustomMessageDialog(message: err);
          });
      return false;
    } else {
      Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
      String err = m['error'].toString();
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return CustomMessageDialog(message: err);
          });
      return false;
    }
  } catch (e) {
    Navigator.pop(context);
    showDialog(
        context: context,
        builder: (context) {
          return const CustomMessageDialog(
              message:
                  " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
        });
    return false;
  }
}

getCurrentUserDataFromServer(BuildContext context) async {
  String token = await getTokenPrefs();
  Map<String, String> headers = {
    "Content-Type": "application/json; charset=utf-8",
    "Accept": "application/json",
    "Authorization": "Bearer" " " + token,
  };

  try {
    var response = await http.get(
        Uri.https("courses.sepidehjelokhani.com", "/api/v2/user-profile"),
        headers: headers);
    if (response.statusCode == 200) {
      Navigator.pop(context);
      Map m = jsonDecode(utf8.decode(response.bodyBytes));
      String name = m['name'].toString();
      String family = m['last_name'].toString();
      int humanType = m['gender'].toString() == "null" ? 2 : m['gender'];
      String imageUrl = m['image'];
      await setUserProfileDataPrefs(name, family, humanType, imageUrl);
    } else if (response.statusCode == 401) {
      Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
      String err = m['error'].toString();
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return CustomMessageDialog(message: err);
          });
    } else {
      return false;
    }
  } catch (e) {
    Navigator.pop(context);
    showDialog(
        context: context,
        builder: (context) {
          return const CustomMessageDialog(
              message:
                  " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
        });
  }
}
