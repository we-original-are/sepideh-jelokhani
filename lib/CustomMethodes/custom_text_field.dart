import 'package:flutter/material.dart';

class CustomTextField extends StatefulWidget {
  const CustomTextField({
    Key key,
      this.hintText,
      this.iconData,
      this.controller,
      this.textInputType,
      this.obscure,
      this.textInputAction,
  }) : super(key: key);
  final String hintText;
  final IconData iconData;
  final TextEditingController controller;
  final TextInputType textInputType;
  final bool obscure;
  final TextInputAction textInputAction;

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool showPassword = true;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
      ),
      child: widget.obscure
          ? TextFormField(
        textInputAction: widget.textInputAction,
        cursorHeight: 20,
        obscureText: showPassword,
        keyboardType: widget.textInputType,
        onChanged: (value) {
          setState(() {
            widget.controller.text = value;
          });
        },
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8.0),
            hintText: widget.hintText,
            suffixIcon: IconButton(
              onPressed: () {
                if (showPassword) {
                  setState(() {
                    showPassword = false;
                  });
                } else {
                  setState(() {
                    showPassword = true;
                  });
                }
              },
              icon: showPassword
                  ? const Icon(Icons.visibility_off_rounded)
                  : const Icon(Icons.visibility_rounded),
            ),
            hintStyle: const TextStyle(
                height: 1.5,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 17),
            prefixIcon: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(100)),
                child: FloatingActionButton(
                  onPressed: null,
                  elevation: 0.0,
                  backgroundColor:
                  const Color.fromARGB(210, 40, 187, 164),
                  child: Icon(widget.iconData),
                )),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50),
                borderSide: const BorderSide(color: Colors.black)),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50),
                borderSide:
                const BorderSide(color: Colors.blue, width: 2)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50))),
      )
          : TextFormField(
        textInputAction: widget.textInputAction,
        keyboardType: widget.textInputType,
        cursorHeight: 20,
        onChanged: (value) {
          setState(() {
            widget.controller.text = value;
          });
        },
        decoration: InputDecoration(
            contentPadding: const EdgeInsets.all(8.0),
            hintText: widget.hintText,
            hintStyle: const TextStyle(
                height: 1.5,
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 17),
            prefixIcon: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    borderRadius: BorderRadius.circular(100)),
                child: FloatingActionButton(
                  onPressed: null,
                  elevation: 0.0,
                  backgroundColor:
                  const Color.fromARGB(210, 40, 187, 164),
                  child: Icon(widget.iconData),
                )),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50),
                borderSide: const BorderSide(color: Colors.black)),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            errorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50),
                borderSide:
                const BorderSide(color: Colors.blue, width: 2)),
            focusedErrorBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(50))),
      ),
    );
  }
}

class CustomAccountTextField extends StatefulWidget {
  const CustomAccountTextField({
    Key key,
      this.hintText,
      this.controller,
      this.labelText,
      this.textInputAction,
  }) : super(key: key);
  final String hintText;
  final String labelText;
  final TextEditingController controller;
  final TextInputAction textInputAction;

  @override
  _CustomAccountTextFieldState createState() => _CustomAccountTextFieldState();
}

class _CustomAccountTextFieldState extends State<CustomAccountTextField> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
      ),
      child: TextFormField(
        textInputAction: widget.textInputAction,
        onChanged: (value) {
          setState(() {
            widget.controller.text = value;
          });
        },
        decoration: InputDecoration(
            labelText: widget.labelText,
            hintText: widget.hintText,
            hintStyle: const TextStyle(
                color: Colors.black54,
                height: 1.5,
                fontWeight: FontWeight.bold,
                fontSize: 18),
            labelStyle: const TextStyle(
                color: Colors.black,
                height: 1.5,
                fontWeight: FontWeight.bold,
                fontSize: 18),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide: const BorderSide(
                  color: Color.fromARGB(210, 40, 187, 164),
                )),
            disabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide:
                const BorderSide(color: Color.fromARGB(210, 40, 187, 164))),
            enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide:
                const BorderSide(color: Color.fromARGB(210, 40, 187, 164))),
            errorBorder:
            OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(8.0),
                borderSide:
                const BorderSide(color: Color.fromARGB(210, 40, 187, 164))),
            focusedErrorBorder:
            OutlineInputBorder(borderRadius: BorderRadius.circular(8.0))),
      ),
    );
  }
}
