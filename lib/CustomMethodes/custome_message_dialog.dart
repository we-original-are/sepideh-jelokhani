import 'package:flutter/material.dart';

class CustomMessageDialog extends StatefulWidget {
  const CustomMessageDialog({Key key,   this.message}) : super(key: key);
  final String message;

  @override
  _CustomMessageDialogState createState() => _CustomMessageDialogState();
}

class _CustomMessageDialogState extends State<CustomMessageDialog> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: showAlertDialog(),
    );
  }

  showAlertDialog() {
    return AlertDialog(
      elevation: 10,
      titlePadding: EdgeInsets.zero,
      title: Container(
        padding: const EdgeInsets.all(8.0),
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10.0),
                topRight: Radius.circular(10.0)),
            color: Color.fromARGB(230, 40, 187, 164)),
        child: const Text(
          "توجه",
          style: TextStyle(color: Colors.white),
        ),
      ),
      content: Text(
        widget.message,
        style: const TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 17,
          height: 1.1,
        ),
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text("باشه"))
      ],
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
    );
  }
}
