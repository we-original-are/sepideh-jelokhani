import 'package:shared_preferences/shared_preferences.dart';

Future setTokenPrefs(String mobile, String token) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.clear();
  await _prefs.setString("token", token);
  await _prefs.setString("mobile", mobile);
  return true;
}

Future setUserProfileDataPrefs(
    String name, String family, int humanType, String imageUrl) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  await _prefs.setString("name", name);
  await _prefs.setString("family", family);
  await _prefs.setInt("humanType", humanType);
  await _prefs.setString("imageUrl", imageUrl);
  return true;
}

Future<Map<String, dynamic>> getUserProfileDataPrefs() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  Map<String, dynamic> _userData = {};

  String name = _prefs.getString('name') ?? "";
  String family = _prefs.getString("family") ?? "";
  int humanType = _prefs.getInt("humanType") ?? 2;
  String imageUrl = _prefs.getString("imageUrl") ?? "";

  _userData = {
    "name": name,
    "family": family,
    "humanType": humanType,
    "imageUrl": imageUrl,
  };

  return _userData;
}

Future getTokenPrefs() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String token = _prefs.getString("token") ?? "";
  return token;
}

Future getUserMobilePrefs() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  String number = _prefs.getString("mobile") ?? "";
  return number;
}

Future deletePrefsDataPrefs() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  try {
    await _prefs.clear();
    return true;
  } catch (e) {
    return false;
  }
}
