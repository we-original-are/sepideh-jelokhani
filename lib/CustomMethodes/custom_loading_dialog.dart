import 'package:flutter/material.dart';

class CustomLoadingDialog extends StatelessWidget {
  const CustomLoadingDialog({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: Colors.transparent,
        body: showAlertDialog(),
      ),
    );
  }

  showAlertDialog() {
    return AlertDialog(
      elevation: 10,
      content: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          CircularProgressIndicator(color: Color.fromARGB(210, 40, 187, 164),),
          SizedBox(
            height: 10,
          ),
          Text(
            "لطفا کمی صبر کنید",
            style: TextStyle(color: Color.fromARGB(210, 40, 187, 164)),
          )
        ],
      ),
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10))),
    );
  }
}
