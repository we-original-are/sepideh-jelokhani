import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_loading_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_text_field.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'package:sepideh_jelokhani/Screens/start_screen.dart';
import 'package:http/http.dart' as http;

import '../CustomMethodes/custome_message_dialog.dart';

class CodeScreen extends StatefulWidget {
  const CodeScreen({Key key,  this.mobile}) : super(key: key);
  final String mobile;

  @override
  _CodeScreenState createState() => _CodeScreenState();
}

class _CodeScreenState extends State<CodeScreen> {
  TextEditingController codeController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal.shade300,
      body: SafeArea(
        child: Center(
          child: Container(
            margin: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  "images/sepideh.png",
                  width: MediaQuery.of(context).size.width / 3,
                  alignment: Alignment.bottomCenter,
                ),
                const SizedBox(
                  height: 8.0,
                ),
                const Text(
                  "برای ورود به سامانه کد پیامک شده به تلفن همراه خود را وارد کنید.",
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 17,
                      fontWeight: FontWeight.bold),
                ),
                CustomTextField(
                  hintText: " کد فعال سازی",
                  iconData: Icons.security,
                  controller: codeController,
                  textInputType: TextInputType.number,
                  obscure: false,
                  textInputAction: TextInputAction.done,
                ),
                const SizedBox(
                  height: 8.0,
                ),
                SizedBox(
                  width: MediaQuery.of(context).size.width,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        primary: const Color.fromARGB(255, 202, 78, 148),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: const [
                          Icon(Icons.arrow_left),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(
                            "ورود",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 17),
                          ),
                        ],
                      ),
                    ),
                    onPressed: () {
                      if (codeController.text.trim().isEmpty ||
                          codeController.text.trim().characters.length < 5 ||
                          codeController.text.trim().characters.length > 5) {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return const CustomMessageDialog(
                                  message:
                                      "لطفا کد فعالسازی پیامک شده را به درستی وارد کنید");
                            });
                      } else {
                        checkActivationCode(
                            widget.mobile, codeController.text.trim());
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  void checkActivationCode(String mobile, String code) async {
    showDialog(
        context: context,
        builder: (context) {
          return const CustomLoadingDialog();
        });
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
    };
    try {
      http.Response response = await http.post(
          Uri.https("courses.sepidehjelokhani.com", "/api/v2/auth/activate"),
          headers: headers,
          body: jsonEncode({"mobile": mobile, "code": code}));
      print(response.statusCode);
      if (response.statusCode == 200) {
        Map m = jsonDecode(utf8.decode(response.bodyBytes));
        String token = m['token'].toString();
        await setTokenPrefs(mobile, token);
        Navigator.pop(context);
        Route route = MaterialPageRoute(builder: (context) {
          return const StartScreen();
        });
        Navigator.pushAndRemoveUntil(context, route, (route) => false);
      } else if (response.statusCode == 401) {
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (context) {
              return const CustomMessageDialog(
                  message: " خطا : کد فعالسازی ارسال شده نادرست است");
            });
      }
    } catch (e) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }
}
