import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sepideh_jelokhani/Auth/code_screen.dart';
import 'package:sepideh_jelokhani/Auth/restore_moblie_screen.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_loading_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_text_field.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custome_message_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'package:sepideh_jelokhani/Screens/start_screen.dart';

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key key}) : super(key: key);


  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  final TextEditingController _controllerMobile = TextEditingController();
  final TextEditingController _controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.teal.shade300,
        body: SafeArea(
          child: Center(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: SingleChildScrollView(
                physics: const BouncingScrollPhysics(),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image.asset(
                      "images/sepideh.png",
                      width: MediaQuery.of(context).size.width / 3,
                      alignment: Alignment.bottomCenter,
                    ),
                    CustomTextField(
                      hintText: " شماره موبایل",
                      iconData: Icons.phone,
                      controller: _controllerMobile,
                      textInputType: TextInputType.phone,
                      obscure: false,
                      textInputAction: TextInputAction.next,
                    ),
                    CustomTextField(
                      hintText: " کلمه عبور",
                      iconData: Icons.security,
                      controller: _controllerPassword,
                      textInputType: TextInputType.visiblePassword,
                      obscure: true,
                      textInputAction: TextInputAction.done,
                    ),
                    const SizedBox(height: 10),
                    costumeElevatedButton("ورود", Icons.login),
                    const Divider(
                      height: 40,
                      color: Colors.white,
                      thickness: 1.5,
                      endIndent: 10.0,
                      indent: 10.0,
                    ),
                    costumeElevatedButton("ثبت نام", Icons.person),
                    const SizedBox(height: 10),
                    TextButton(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: const [
                          Icon(
                            Icons.wifi_protected_setup,
                            color: Colors.white,
                          ),
                          SizedBox(width: 5.0),
                          Text(
                            "کلمه عبورم را فراموش کردم",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold,
                                fontSize: 17),
                          )
                        ],
                      ),
                      onPressed: () {
                        Route route = MaterialPageRoute(builder: (context) {
                          return  RestMobileScreen();
                        });
                        Navigator.push(context, route);
                      },
                    ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }

  costumeElevatedButton(String text, IconData iconData) {
    String pattern = r'(^09[0-9]{9})';
    RegExp regExp = RegExp(pattern);
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: const Color.fromARGB(255, 202, 78, 148),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50))),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(
                iconData,
                color: Colors.white,
              ),
              const SizedBox(
                width: 5.0,
              ),
              Text(
                text,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ],
          ),
        ),
        onPressed: () {
          if (_controllerMobile.text.trim().isEmpty &&
              _controllerPassword.text.trim().isEmpty) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message:
                          "لطفا شماره موبایل و کلمه عبور خود را وارد کنید");
                });
          } else if (_controllerMobile.text.trim().isEmpty) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message: "لطفا شماره موبایل خود را وارد کنید");
                });
          } else if (!regExp.hasMatch(_controllerMobile.text.trim())) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message:
                          "لطفا شماره همراه خود را به درستی وارد کنید\nمانند : 09123456789");
                });
          } else if (_controllerPassword.text.trim().isEmpty) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message: "لطفا کلمه عبور خود را وارد کنید");
                });
          } else if (_controllerPassword.text.trim().characters.length < 6) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message:
                          "کلمه عبور باید حداقل دارای 6 کاراکتر(حرف یا عدد) باشد");
                });
          } else {
            sendUserCode(_controllerMobile.text.trim(),
                _controllerPassword.text.trim(), context);
          }
        },
      ),
    );
  }

  Future sendUserCode(
      String mobile, String password, BuildContext context) async {
    showDialog(
        context: context,
        builder: (context) {
          return const CustomLoadingDialog();
        });
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
    };
    try {
      http.Response response = await http.post(
          Uri.https(
              "courses.sepidehjelokhani.com", "/api/v2/auth/authenticate"),
          headers: headers,
          body: jsonEncode({"mobile": mobile, "password": password}));

      print(response.statusCode);
      if (response.statusCode == 201) {
        Navigator.pop(context);
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => CodeScreen(
                      mobile: mobile,
                    )));
      } else if (response.statusCode == 200) {
        Map m = jsonDecode(utf8.decode(response.bodyBytes));
        String token = m['token'].toString();
        await setTokenPrefs(mobile, token);
        await _getUserProfileDataFromServer(token);
        Navigator.pop(context);
        Route route = MaterialPageRoute(builder: (BuildContext context) {
          return const StartScreen();
        });
        Navigator.pushReplacement(context, route);
      } else if (response.statusCode == 401) {
        Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
        String err = m['error'].toString();
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (context) {
              return CustomMessageDialog(message: err);
            });
      } else {
        Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
        String err = m['error'].toString();
        Navigator.pop(context);

        showDialog(
            context: context,
            builder: (context) {
              return CustomMessageDialog(
                  message: err);
            });
      }
    } catch (e) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }

  _getUserProfileDataFromServer(String token) async {
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "Authorization": "Bearer" " " + token,
    };
    try {
      var response = await http.get(
          Uri.https("courses.sepidehjelokhani.com", "/api/v2/user-profile"),
          headers: headers);
      if (response.statusCode == 200) {
        Map m = jsonDecode(utf8.decode(response.bodyBytes));
        String name = m['name'].toString();
        String family = m['last_name'].toString();
        int humanType = m['gender'].toString() == "null" ? 2 : m['gender'];
        String imageUrl = m['image'];
        await setUserProfileDataPrefs(name, family, humanType, imageUrl);
      } else if (response.statusCode == 401) {
        Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
        String err = m['error'].toString();
        showDialog(
            context: context,
            builder: (context) {
              return CustomMessageDialog(message: err);
            });
      } else {
        return false;
      }
    } catch (e) {
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }
}
