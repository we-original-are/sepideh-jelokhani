import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:sepideh_jelokhani/Auth/set_code_and_new_password.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_loading_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_text_field.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custome_message_dialog.dart';
import 'package:http/http.dart' as http;

class RestMobileScreen extends StatefulWidget {
  const RestMobileScreen({Key key}) : super(key: key);

  @override
  _RestMobileScreenState createState() => _RestMobileScreenState();
}

class _RestMobileScreenState extends State<RestMobileScreen> {
  final TextEditingController _mobileController = TextEditingController();
  String _mobileNumber = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal.shade300,
      body: SafeArea(
          child: Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "images/sepideh.png",
                width: MediaQuery.of(context).size.width / 3,
                alignment: Alignment.bottomCenter,
              ),
              const SizedBox(
                height: 8.0,
              ),
              const Text(
                "شماره موبایل خود را برای بازیابی کلمه عبور وارد کنید.",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
              CustomTextField(
                hintText: " شماره موبایل",
                iconData: Icons.phone,
                controller: _mobileController,
                textInputType: TextInputType.phone,
                obscure: false,
                textInputAction: TextInputAction.done,
              ),
              const SizedBox(height: 10),
              costumeElevatedButton()
            ],
          ),
        ),
      )),
    );
  }

  costumeElevatedButton() {
    String pattern = r'(^09[0-9]{9})';
    RegExp regExp = RegExp(pattern);
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
            primary: const Color.fromARGB(255, 202, 78, 148),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50))),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Icon(
                Icons.login,
                color: Colors.white,
              ),
              SizedBox(
                width: 5.0,
              ),
              Text(
                "تایید",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
              ),
            ],
          ),
        ),
        onPressed: () {
          if (_mobileController.text.trim().isEmpty) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message: "لطفا ابتدا شماره موبایل خود را وارد کنید");
                });
          } else if (!regExp.hasMatch(_mobileController.text.trim())) {
            showDialog(
                context: context,
                builder: (context) {
                  return const CustomMessageDialog(
                      message:
                          "لطفا شماره همراه خود را به درستی وارد کنید\nمانند : 09123456789");
                });
          } else {
            setState(() {
              _mobileNumber = _mobileController.text.trim();
            });
            _sendUserCodeJustWithMobile();
          }
        },
      ),
    );
  }

  void _sendUserCodeJustWithMobile() async {
    showDialog(
        context: context,
        builder: (context) {
          return const CustomLoadingDialog();
        });
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
    };
    try {
      http.Response response = await http.post(
          Uri.https("courses.sepidehjelokhani.com", "/api/v2/forgot/forgot"),
          headers: headers,
          body: jsonEncode({"mobile": _mobileNumber}));
      print(response.statusCode);
      if (response.statusCode == 200) {
        Navigator.pop(context);
        Route route = MaterialPageRoute(builder: (BuildContext context) {
          return SetCodeAndPassForget(mobile: _mobileNumber);
        });
        Navigator.push(context, route);
      } else if (response.statusCode == 401) {
        Navigator.pop(context);

        Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
        String err = m['error'].toString();
        showDialog(
            context: context,
            builder: (context) {
              return CustomMessageDialog(message: err);
            });
      }
    } catch (e) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }
}
