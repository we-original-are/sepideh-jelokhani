import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:sepideh_jelokhani/CustomMethodes/custom_loading_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custom_text_field.dart';
import 'package:sepideh_jelokhani/CustomMethodes/custome_message_dialog.dart';
import 'package:sepideh_jelokhani/CustomMethodes/manage_prefs.dart';
import 'package:sepideh_jelokhani/Screens/start_screen.dart';

import 'auth_screen.dart';

class SetCodeAndPassForget extends StatefulWidget {
  const SetCodeAndPassForget({Key key,  this.mobile})
      : super(key: key);
  final String mobile;

  @override
  _SetCodeAndPassForgetState createState() => _SetCodeAndPassForgetState();
}

class _SetCodeAndPassForgetState extends State<SetCodeAndPassForget> {
  final TextEditingController _restoreCodeController = TextEditingController();
  final TextEditingController _restorePasswordController =
      TextEditingController();
  String _userCode = "";
  String _userPassword = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.teal.shade300,
      body: SafeArea(
          child: Center(
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset(
                "images/sepideh.png",
                width: MediaQuery.of(context).size.width / 3,
                alignment: Alignment.bottomCenter,
              ),
              const SizedBox(
                height: 8.0,
              ),
              const Text(
                "لطفا کد بازیابی و کلمه عبور جدید خود را وارد کنید.",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold),
              ),
              CustomTextField(
                hintText: " کد بازیابی",
                iconData: Icons.restore,
                controller: _restoreCodeController,
                textInputType: TextInputType.number,
                obscure: false,
                textInputAction: TextInputAction.next,
              ),
              CustomTextField(
                hintText: " کلمه عبور جدید",
                iconData: Icons.security,
                controller: _restorePasswordController,
                textInputType: TextInputType.visiblePassword,
                obscure: true,
                textInputAction: TextInputAction.done,
              ),
              const SizedBox(
                height: 10,
              ),
              _customElevatedButton("تایید", Icons.login)
            ],
          ),
        ),
      )),
    );
  }

  _customElevatedButton(String text, IconData iconData) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: const Color.fromARGB(255, 202, 78, 148),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  iconData,
                  color: Colors.white,
                ),
                const SizedBox(
                  width: 5.0,
                ),
                Text(
                  text,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold, fontSize: 17),
                ),
              ],
            ),
          ),
          onPressed: () {
            if (_restorePasswordController.text.trim().isEmpty &&
                _restoreCodeController.text.trim().isEmpty) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const CustomMessageDialog(
                        message:
                            "لطفا کد بازیابی ارسال شده روی گوشی خود و کلمه عبور جدید خود را وارد کنید");
                  });
            } else if (_restoreCodeController.text.trim().isEmpty) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const CustomMessageDialog(
                        message: "لطفا کد بازیابی دریافت شده را وارد کنید");
                  });
            } else if (_restoreCodeController.text.trim().characters.length >
                    5 ||
                _restoreCodeController.text.trim().length < 5) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const CustomMessageDialog(
                        message:
                            "لطفا کد بازیابی دریافت شده را به درستی وارد کنید");
                  });
            } else if (_restorePasswordController.text.trim().isEmpty) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const CustomMessageDialog(
                        message: "لطفا کلمه عبور جدید خود را وارد کنید");
                  });
            } else if (_restorePasswordController.text
                    .trim()
                    .characters
                    .length <
                6) {
              showDialog(
                  context: context,
                  builder: (context) {
                    return const CustomMessageDialog(
                        message:
                            "کلمه عبور باید حداقل دارای 6 کاراکتر(حرف یا عدد) باشد");
                  });
            } else {
              setState(() {
                _userCode = _restoreCodeController.text.trim();
                _userPassword = _restorePasswordController.text.trim();
              });
              _sendUserCodeAndNewPassword();
            }
          }),
    );
  }

  void _sendUserCodeAndNewPassword() async {
    showDialog(
        context: context,
        builder: (context) {
          return const CustomLoadingDialog();
        });
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
    };
    try {
      http.Response response = await http.post(
          Uri.https("courses.sepidehjelokhani.com", "/api/v2/forgot/verify"),
          headers: headers,
          body: jsonEncode({"mobile": widget.mobile, "code": _userCode}));
      if (response.statusCode == 200) {
        Map m = jsonDecode(utf8.decode(response.bodyBytes));
        String token = m['token'].toString();
        finalMethodSetUser(token);
      } else if (response.statusCode == 401) {
        Map<String, dynamic> m = jsonDecode(utf8.decode(response.bodyBytes));
        String err = m['error'].toString();
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (context) {
              return CustomMessageDialog(message: err);
            });
      } else {
        Navigator.pop(context);
      }
    } catch (e) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }

  finalMethodSetUser(String token) async {
    Map<String, String> headers = {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "Authorization": "Bearer" " " + token
    };
    try {
      http.Response response = await http.post(
          Uri.https("courses.sepidehjelokhani.com", "/api/v2/forgot/reset"),
          headers: headers,
          body: jsonEncode({"password": _userPassword}));
      if (response.statusCode == 200) {
        await setTokenPrefs(widget.mobile, token);
        Navigator.pop(context);
        Route route = MaterialPageRoute(builder: (BuildContext context) {
          return const AuthScreen();
        });
        Navigator.pushAndRemoveUntil(context, route, (route) => false);
        ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(
            content: Text(
              'رمز عبور با موفقیت تغییر یافت',
              style: TextStyle(color: Colors.white),
            ),
          ),
        );
      } else {
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (context) {
              return const CustomMessageDialog(
                  message:
                      " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
            });
      }
    } catch (e) {
      Navigator.pop(context);
      showDialog(
          context: context,
          builder: (context) {
            return const CustomMessageDialog(
                message:
                    " خطای برقراری ارتباط با سرور \n لطفا پس از اطمینان از متصل بودن به اینترنت ، از غیر فعال بودن vpn خود مطمئن شوید. ");
          });
    }
  }
}
